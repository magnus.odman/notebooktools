from nb2script.core import convert_to_script, run_notebook
import fire

def main():
    fire.Fire(convert_to_script)
    
def run():
    fire.Fire(run_notebook)
