Tools for developing python projects in notebooks (inspired by the fastai workflow)

Installation
============

pip install -U git+https://gitlab.com/magnus.odman/notebooktools.git



Usage
=====

Prefix the code blocks you want to export

```python
#export

def my_exported_function(a):
    return a + 1
```

Run nb2script to export to code from notebook. The code in example.ipynb will be exported to example.py.

```bash
nb2script exapmle.ipynb
```

The exported script can then be used in python projects.

```python
from example import my_exported_function

assert 2 == my_exported_function(1)
```

