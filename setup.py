import setuptools
with open("README.md", "r") as fh:
    long_description = fh.read()
setuptools.setup(
     name='notebooktools',  
     version='0.1.1',
     scripts=[] ,
     author="Magnus Ödman",
     author_email="magnus.odman@gmail.com",
     description="Tools for developing python projects in notebooks",
     long_description=long_description,
     long_description_content_type="text/markdown",
     url="",
     packages=setuptools.find_packages(),
     install_requires=[
          'fire',
          'nbformat',
          'nbconvert'
     ],
     entry_points = {
         'console_scripts': [
             'nb2script=nb2script.command_line:main',
             'nbrun=nb2script.command_line:run'
         ],
     },
     classifiers=[
         "Programming Language :: Python :: 3",
         "License :: OSI Approved :: MIT License",
         "Operating System :: OS Independent",
     ],
)

